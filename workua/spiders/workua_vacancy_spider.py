import scrapy
from scrapy import Selector
from workua.items import WorkuaVacancyItem


class WorkuaVacancySpider(scrapy.Spider):
    name = "workuavacancy"
    start_urls = [
        'https://www.work.ua/jobs/3503931/',
    ]

    def parse(self, response):
        root = Selector(response)
        vacancy = root.xpath('//div[@class = "card wordwrap"]')
        for info in vacancy:
            item = WorkuaVacancyItem()
            item['date'] = info.xpath('//p[@class = "cut-bottom-print"]/span/text()').extract()
            item['title'] = info.xpath('//h1[@id = "h1-name"]/text()').extract()
            item['salary'] = info.xpath('//h3[@class = "text-muted text-muted-print"]/b/text()').extract()
            item['company'] = info.xpath('//dl[@class = "dl-horizontal"]/dd/a/b/text()').extract()
            item['city'] = info.xpath('//dl[@class = "dl-horizontal"]/dd[2]/text()').extract()
            #item['description'] = info.xpath('//p[]').extract()
            yield item




    #def parse(self, response):
     #   for i in response.xpath('//h1[@class = "add-top-sm"]/text()').extract():
      #      print(i)


