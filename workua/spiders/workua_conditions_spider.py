import scrapy
from scrapy import Selector
from workua.items import WorkuaConditionsItem


class WorkuaConditionsSpider(scrapy.Spider):
    name = "workuaconditions"
    start_urls = [
        'https://www.work.ua/about-us/conditions/',
    ]

    def parse(self, response):
        root = Selector(response)
        conditions = root.xpath('//div[@class = "card"]')
        item = WorkuaConditionsItem()
        item['text'] = conditions.xpath('//h2[]/text()').extract()
        yield item